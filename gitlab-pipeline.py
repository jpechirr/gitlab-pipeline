import os
import argparse
import subprocess
import json
import time
import sys


# Address of GitLab host
HOST = 'gitlab.cern.ch'
# GitLab API version
API_VERSION = 'v4'
# Interval between pipeline status requests in seconds
REFRESH_INTERVAL = 1

# Successful run code
EXIT_SUCCESS = 0
# Generic error code
EXIT_ERROR = 1


def parse_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--id", help="Target project ID", required=True)
    parser.add_argument("-s", "--secret", help="GitLab Secret API Token", required=True)
    parser.add_argument("-t", "--trigger", help="GitLab Pipeline Trigger API Token", required=True)
    parser.add_argument("-p", "--variables", help="Key-value pairs for pipeline varibales; e.g: -p VAR1=value VAR2=value", nargs='*')
    parser.add_argument("-r", "--ref", help="Target project ref (branch/tag)", default="master")
    parser.add_argument("-o", "--timeout", help="Timeout in seconds", default=120, type=int)
    args = parser.parse_args()
    return args


def main():
    """Triggers a pipeline on a target project and returns whether it was successful or not"""

    # Arguments
    args = parse_args()
    trigger_token = args.trigger
    proj_id = args.id
    secret_token = args.secret
    ref = args.ref
    variables = args.variables

    # Issue command to trigger pipeline
    cmd = f'curl --silent -X POST -F token={trigger_token} -F ref={ref} https://{HOST}/api/{API_VERSION}/projects/{proj_id}/trigger/pipeline'
    split_cmd = cmd.split(" ")
    # Append additional args (e.g. CI variables for the target pipeline) if needed
    if variables is not None:
        for var in variables:
            key, value = var.split("=")
            split_cmd += ['-F', f'variables[{key}]={value}']

    out = subprocess.check_output(split_cmd)
    response = json.loads(out)

    try:
        pipeline_id = response["id"]
    except KeyError as e:
        return EXIT_ERROR

    # Create command to check pipeline status
    cmd = f'curl --silent --header PRIVATE-TOKEN:{secret_token} https://gitlab.cern.ch/api/v4/projects/{proj_id}/pipelines/{pipeline_id}'
    split_cmd = cmd.split(" ")

    # Compute the timeout moment
    timeout = time.time() + args.timeout

    # Request pipeline status until complete or timeout occurs
    done = False
    pipeline_status = "failed"
    while not done:

        # Query GitLab API for pipeline status
        time.sleep(REFRESH_INTERVAL)
        out = subprocess.check_output(split_cmd)
        response = json.loads(out)
        
        try:
            pipeline_status = response['status']
        except KeyError as e:
            return EXIT_ERROR

        if pipeline_status not in {"pending", "running"} or time.time() > timeout:
            done = True

    if response['status'] != "success":      
        return EXIT_ERROR

    return EXIT_SUCCESS


if __name__ == '__main__':
    err = main()
    sys.exit(err)
